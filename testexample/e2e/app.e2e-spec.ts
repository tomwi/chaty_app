import { TestexamplePage } from './app.po';

describe('testexample App', function() {
  let page: TestexamplePage;

  beforeEach(() => {
    page = new TestexamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
