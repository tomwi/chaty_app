import { MyangularprojectPage } from './app.po';

describe('myangularproject App', function() {
  let page: MyangularprojectPage;

  beforeEach(() => {
    page = new MyangularprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
